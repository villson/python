import json


def check_for_product_and_coupon(json_string):
    try:
        
        parsed_data = json.loads(json_string)
        
       
        if 'objects' in parsed_data['result']:
            
            for obj in parsed_data['result']['objects']:
                if obj['productId'] == "21095":
                    for detail in obj['priceDetails']['discountDetails']:
                        if detail['couponCode'] == "comp10241a21095":
                            return True
        return False
    except json.JSONDecodeError:
        return False


file_path = 'response.txt'


with open(file_path, 'r') as file:
    lines = file.readlines()  #
for i, line in enumerate(lines):
    if check_for_product_and_coupon(line):
        
        print(f"Строка {i}: {lines[i-1].strip()}")
        #print(f"Строка {i+1}: {line.strip()}")